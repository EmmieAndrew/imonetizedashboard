const Auth = require("../app/controllers/AuthController");
const User = require("../app/controllers/UserController");
const Crate = require("../app/controllers/CrateController");
const verifyToken = require("../app/middleware/auth");

var multer = require("multer");
var upload = multer({ dest: "upload/" });
// var fs = require('fs');

/**
 * To be shifted in main code
 */
var CryptoJS = require("crypto-js");

module.exports = (app) => {
  app.get("/", (req, res) => {
    res.json({
      info: "Node.js, Express, and Postgres API",
    });
  });

  /**
   * Auth Routes
   */
  app.post("/register", Auth.register);
  app.post("/verifyOtp", Auth.verifyOtp);
  app.post("/login", Auth.login);
  app.get("/gettoken", Auth.get_token);
  app.post("/verifyLogin", Auth.verifyLogin);
  app.post("/activateUser", Auth.activateUser);
  // app.post("/personalDetails", Auth.personalDetails);
  // app.post("/updateProfile", Auth.updateProfile);
  // app.post("/addAddress", Auth.addAddress);

  /**
   * Profile Routes
   */
  app.get("/profile", [verifyToken], (req, res) => {
    res.json({
      id: req.userId,
    });
  });

  app.post("/getUserInfo", User.getUserInfo);
  app.post("/addUpdateAddress", User.addUpdateAddress);
  app.post("/addUpdateDetails", User.addUpdateDetails);
  app.post("/getAddresses", User.getAddresses);
  app.post("/getProfile", User.getProfile);
  app.get("/getUsers", User.getUsers);

   /**
   * Crate Routes
   */

    app.post("/addUpdateCrate", Crate.addUpdateCrate);
    app.post("/getCrates", Crate.getCrates);

    // app.post(
    //   "/upload-images",
    //   upload.single("recfile"),
    //   MediaController.uploadImages
    // );
};

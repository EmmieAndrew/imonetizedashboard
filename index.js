const cors = require("cors");
const express = require('express'),
      app = express(),
      http = require('http'),
      port = 8080;

const server = http.Server(app).listen(port),
    socketIo = require('socket.io'),
    io = socketIo(server);
server.timeout = 1000;
console.log('Server running at http://154.53.32.142:8080/');

const routes = require('./routes')

// app.use(bodyParser.json())
// app.use(
//     bodyParser.urlencoded({
//         extended: true
//     })
// )

// for parsing application/json
app.use(express.json());
app.use(cors());
// for parsing application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// for parsing multipart/form-data
// app.use(upload.array());
app.use(express.static('public'));

routes(app)

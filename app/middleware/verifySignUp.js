const Auth = require('../models/Auth')

const checkUnique = (req, res, next) => {
    Auth.isExists(req.body.email, total => {
        if (total) {
            res.status(400).send({
                msg: 'Failed! Email is already exists.'
            });
        } else {
            next();
        }
    })
}

module.exports = checkUnique

const Crate = require("../models/Crate");

const addUpdateCrate = (req, res) => {
    Crate.addUpdateCrate(req.body, (err, success) => {
      if (err) {
        res.status(400).send(err);
      } else {
        success.code = "200";
        res.send(success);
      }
    });
  };

  const getCrates = (req, res) => {
    Crate.getCrates(req.body, (err, success) => {
      if (err) {
        res.status(400).send(err);
      } else {
        success.code = "200";
        res.send(success);
      }
    });
  };

  module.exports = {
    addUpdateCrate,
    getCrates
};
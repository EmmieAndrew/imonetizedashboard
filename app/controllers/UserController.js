const User = require("../models/User");

const getUserInfo = (req, res) => {
    User.getUserInfo(req.body, (err, success) => {
      if (err) {
        res.status(400).send(err);
      } else {
        success.code = "200";
        res.send(success);
      }
    });
  };

  const addUpdateAddress = (req, res) => {
    User.addUpdateAddress(req.body, (err, success) => {
      if (err) {
        res.status(400).send(err);
      } else {
        success.code = "200";
        res.send(success);
      }
    });
  };

  const addUpdateDetails = (req, res) => {
    User.addUpdateDetails(req.body, (err, success) => {
      if (err) {
        res.status(400).send(err);
      } else {
        success.code = "200";
        res.send(success);
      }
    });
  };

  const getAddresses = (req, res) => {
    User.getAddresses(req.body, (err, success) => {
      if (err) {
        res.status(400).send(err);
      } else {
        success.code = "200";
        res.send(success);
      }
    });
  };

  const getProfile = (req, res) => {
    User.getProfile(req.body, (err, success) => {
      if (err) {
        res.status(400).send(err);
      } else {
        success.code = "200";
        res.send(success);
      }
    });
  };

  const getUsers = (req, res) => {
    User.getUsers(req.body, (err, success) => {
      if (err) {
        res.status(400).send(err);
      } else {
        success.code = "200";
        res.send(success);
      }
    });
  };

module.exports = {
    getUserInfo,
    addUpdateAddress,
    addUpdateDetails,
    getAddresses,
    getProfile,
    getUsers
}
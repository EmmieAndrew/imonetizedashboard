const Auth = require("../models/Auth");

const register = (req, res) => {
  Auth.register(req.body, (err, success) => {
    if (err) {
      res.status(400).send(err);
    } else {
      success.code = "200";
      res.send(success);
    }
  });
};

const verifyOtp = (req, res) => {
    Auth.verifyOtp(req.body, (err, success) => {
      if (err) {
        res.status(400).send(err);
      } else {
        success.code = "200";
        res.send(success);
      }
    });
  };

const login = (req, res) => {
  Auth.login(req.body, (err, success) => {
    if (err) {
      res.status(400).send(err);
    } else {
      success.code = "200";
      res.send(success);
    }
  });
};

const verifyLogin = (req, res) => {
    Auth.verifyLogin(req.body, (err, success) => {
      if (err) {
        res.status(400).send(err);
      } else {
        success.code = "200";
        res.send(success);
      }
    });
};

const updateProfile = (req, res) => {
    Auth.updateProfile(req.body, (err, success) => {
      if (err) {
        res.status(400).send(err);
      } else {
        success.code = "200";
        res.send(success);
      }
    });
  };

  const personalDetails = (req, res) => {
    Auth.personalDetails(req.body, (err, success) => {
      if (err) {
        res.status(400).send(err);
      } else {
        success.code = "200";
        res.send(success);
      }
    });
  };

  const addAddress = (req, res) => {
    Auth.addAddress(req.body, (err, success) => {
      if (err) {
        res.status(400).send(err);
      } else {
        success.code = "200";
        res.send(success);
      }
    });
  };

const get_token = (req, res) => {
    Auth.gettoken(req.body, (err, success) => {
      if (err) {
        res.status(400).send(err);
      } else {
        success.code = "200";
        res.send(success);
      }
    });
};

const activateUser = (req, res) => {
  Auth.activateUser(req.body, (err, success) => {
    if (err) {
      res.status(400).send(err);
    } else {
      success.code = "200";
      res.send(success);
    }
  });
};

module.exports = {
    register,
    login,
    get_token,
    verifyOtp,
    verifyLogin,
    updateProfile,
    personalDetails,
    addAddress,
    activateUser
};
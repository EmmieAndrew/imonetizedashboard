const pool = require('../../config/db')

const getUserInfo = async (params, done) => {
    const { user_id } = params;
    let user = await pool.query('SELECT * FROM users WHERE user_id = $1 ', [user_id])
    let students = await pool.query('SELECT * FROM students WHERE user_id = $1 ', [user_id])
    let addresses = await pool.query('SELECT * FROM addresses WHERE user_id = $1 ', [user_id])
    let data = [];
    let details = {
        user: user.rows[0],
        students: students.rows,
        addresses: addresses.rows
    }
    data.push(details)
    done(null, {
        msg: "Details Fetched Successfully.",
        userDetails: data,
      });

}

const addUpdateAddress = (params, done) => {
    pool.query("select * from addUpdateAddress($1)", [params], (err, res) => {
    if (err) throw err;
    done(null, res.rows[0]);
    });
  }

  const addUpdateDetails = (params, done) => {
    pool.query("select * from addUpdateDetails($1)", [params], (err, res) => {
    if (err) throw err;
    done(null, res.rows[0]);
    });
  }

  const getAddresses = async (params, done) => {
    const { user_id } = params;
    let addresses = await pool.query('SELECT * FROM addresses WHERE user_id = $1 ', [user_id])
    done(null, {
      msg: "Details Fetched Successfully.",
      address:addresses.rows
    });
  }

  const getProfile = async (params, done) => {
    const { user_id } = params;
    pool.query(
      'SELECT * FROM users WHERE id = ?',user_id,
      (err, res) => {
        if (err) throw err;
        done(null, {
          msg: "Details fetched successfully.",
          user: res[0]
        });
      }
    );
  }

  const getUsers = (params, done) => {
    pool.query(
      'SELECT * FROM users WHERE role_id = 1 ORDER BY id DESC',
      (err, res) => {
        if (err) throw err;
        done(null, {
          msg: "All users fetched successfully.",
          users: res
        });
      }
    );
  }

module.exports = {
    getUserInfo,
    addUpdateAddress,
    addUpdateDetails,
    getAddresses,
    getProfile,
    getUsers
}

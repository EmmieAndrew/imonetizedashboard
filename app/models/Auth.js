const pool = require("../../config/db");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { secret } = require("../../config/auth.config");

const nodemailer = require("nodemailer");
const ejs = require("ejs");

var FROM_ADDRESS = 'emmieandrewwork@gmail.com';
var SUBJECT = "Welcome to Imonetize";

const isExistsId = (id, next) => {
  pool.query(
    'SELECT * FROM users WHERE id = ?',id,
    (err, res) => {
    if (err) throw err;

      let response = {
        total: res.length,
        user: res[0],
      };

      if (res.rowCount) 
      response.user = res.rows[0];
      next(response);
  });
};

const isExists = (email, next) => {
  pool.query(
    'SELECT * FROM users WHERE email = ? AND status = "1"',email,
    (err, res) => {
      if (err) throw err;

      let response = {
        total: res.length,
        // status: res[0].status,
        user: res[0],
      };

      if (res.rowCount) 
      response.user = res.rows[0];
      next(response);
    }
  );
};

const register = async (params, done) => {
  let { name, email, password, phone_no } = params;
  isExists(email, (user) => {
    if (user.total) {
      done(
        { msg: "Email already exists, please try with another Email." },
        null
      );
    } else {
      var hashpassword = bcrypt.hashSync(password, 8);
      var current_date = new Date();
      pool.query("INSERT INTO users (name, email, password, phone_no,created_at) values ('" + name + "', '" + email + "', '" + hashpassword + "', '" + phone_no + "', '" + current_date + "')",
        async (err, res) => {
          // sendMail(email, (user) => {})
          sendConfirmationEmail(email,name);
          if (err) throw err;
          done(null, {
            msg: "Registration success!!",
          });
        }
      );
    }
  });
};


const login = (params, done) => {
  let { email, password } = params;
  isExists(email, (user) => {
    if (!user.total) {
      done(
        {
          msg: "Failed! User not found.",
        },
        null
      );
    } else {
      user = user.user;
      var passwordMatched = bcrypt.compareSync(password, user.password);

      if (!passwordMatched) {
        done(
          {
            msg: "Failed! Password not matched.",
          },
          null
        );
      } else {
        var token = jwt.sign({ id: user.id }, secret, {
          expiresIn: 86400 * 365, // 24 hours
        });

        done(null, {
          msg: "Success! Yo're logged in.",
          // accessToken: token,
          data: user,
        });
      }
    }
  });
};

const smtpTransport = nodemailer.createTransport({
  host: 'mail.gmail.com',
  // port: 465,
  service: "Gmail",
  secure: true,
  auth: {
      user: "emmieandrewwork@gmail.com",
      pass: "lhbxxjikhpslyxvu"
  }
});

const sendVerificationEmail = (email,name) => {
  console.log("Check");
  smtpTransport.sendMail({
    from: "admin@imonetize.com",
    to: email,
    subject: "Welcome to imonetize",
    html: `<h1>Account Verification Confirmation</h1>
        <h2>Hello ${name}</h2>
        <p>You are verified so lets get login with your credentials</p>
        <p>Thanks & Regards !!</p>
        </div>`,
  }).catch(err => console.log(err));
};

const sendConfirmationEmail = (email,name) => {
  console.log("Check");
  smtpTransport.sendMail({
    from: "admin@imonetize.com",
    to: email,
    subject: "Welcome to imonetize",
    html: `<h1>Account Registration Confirmation</h1>
        <h2>Hello ${name}</h2>
        <p>Thank you for registration. You will be inform soon when you get verified </p>
        <p>Thanks & Regards !!</p>
        </div>`,
  }).catch(err => console.log(err));
};



// const sendMail = (email, next) => {
//   // var template = __dirname + "/test.ejs"
  
//   ejs.renderFile(__dirname + "/emails/share_email.ejs", { name: "Joe Due", subject: SUBJECT }, (err, data) => {
//     console.log(__dirname);
//     if(err) console.log(err);
//       else {
//           var mailOptions = {
//               from: "IMONETIZE <" + FROM_ADDRESS + ">",
//               to: email,
//               replyTo: FROM_ADDRESS,
//               subject: SUBJECT,
//               html: data
//           };

//           smtpTransport.sendMail(mailOptions, (err, info) => {
//               if (err) {
//                   console.log(err);
//                   res.send({ error: 1, msg: err });
//               } else {
//                 next(response);
//                   // console.log('Message sent: ' + info.response);
//                   // res.send({ error: 0, 'msg': info.response })
//               }
//           })
//       }
//   })
// }

const activateUser = (params, done) => {
  let { user_id } = params;
  isExistsId(user_id, (user) => {
    if (!user.total) {
      done(
        {
          msg: "Failed! User not found.",
        },
        null
      );
    } else {
      var checkUser = user;
      if(checkUser.user.status == 0){
       pool.query("UPDATE users SET status='1' WHERE id='" + user_id + "'");
       sendVerificationEmail(checkUser.user.email,checkUser.user.name);
        done(null, {
          msg: 'Success! User Is Verified.',
        });
      }else{
        pool.query("UPDATE users SET status='0' WHERE id='" + user_id + "'");
        done(null, {
          msg: 'Success! User Is Deactivated.',
        });
      }
    }
  });
}

module.exports = {
  isExists,
  register,
  login,
  sendConfirmationEmail,
  sendVerificationEmail,
  activateUser
};
